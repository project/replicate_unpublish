<?php

namespace Drupal\replicate_unpublish\EventSubscriber;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\replicate\Events\ReplicateAlterEvent;
use Drupal\replicate\Events\ReplicatorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Makes  the node unpublished after it replicated.
 */
class ReplicateUnpublishNode implements EventSubscriberInterface {

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a \Drupal\replicate_unpublish\EventSubscriber\RedirectRequestSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface
   *   The language manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * Sets the status of a replicated node to unpublished.
   *
   * @param \Drupal\replicate\Events\ReplicateAlterEvent $event
   *  The event fired by the replicator.
   *  For more details look at replicate_api doc.
   *
   */
  public function setUnpublished(ReplicateAlterEvent $event) {
    $cloned_entity = $event->getEntity();

    if (!$cloned_entity instanceof Node) {
      return;
    }

    // Get languages enable.
    $languages = $this->languageManager->getLanguages();
    foreach ($languages as $lang_code => $language) {
      // Check if entity has translation by lang_code and unpublished.
      if ($cloned_entity->hasTranslation($lang_code)) {
        $cloned_entity_translation = $cloned_entity->getTranslation($lang_code);
        // Set node translation as unpublished.
        $cloned_entity_translation->set('status', Node::NOT_PUBLISHED);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[ReplicatorEvents::REPLICATE_ALTER][] = 'setUnpublished';
    return $events;
  }

}
