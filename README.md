###Replicate Unpublished

Depends on the Replicate API and Replicate UI Module. While Replicating Nodes using Replicate API and Replicate UI Module this will unpublished the node.

Most of the time it is painful when replicating ( clone) a node it gets published immediately. first cons are Duplicate date on your site, furthermore, if you have some action on node published event it will trigger when you're replicating.

This module Make the node unpublished on Replication.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/replicate_unpublished

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/replicate_unpublished


## REQUIREMENTS

No special requirements.


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

No configuration is needed.


## MAINTAINERS

Current maintainers:
 * Yusef Mohamadi (yuseferi) - https://www.drupal.org/u/yuseferi
